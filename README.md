# uniapp + uView1.8.7 通过vue-cli方式运行项目

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### 自定义配置
See [Configuration Reference](https://cli.vuejs.org/config/).


## 云闪付小程序
**开发时运行以下命令，启动项目**
```shell
npm run dev:custom mp-unionpay
```

**部署到服务端**
```shell
npm run build:custom mp-unionpay
```

**条件编译的使用**
```js
// #ifdef MP-UNIONPAY
...
// #endif
```


**启用压缩的方法**
- 在`package.json`中添加参数`--minimize`，示例：`"dev:mp-weixin": "cross-env NODE_ENV=development UNI_PLATFORM=mp-weixin vue-cli-service uni-build --watch --minimize"`

**无痛刷新token**
- 默认写在`utils/http.interceptor.js`中，可以参考该页面的代码，简单修改即可使用
- 修改一处uView-ui组件内部的方法，在`uview-ui/libs/request/index.js`文件中的第38行

**解决H5端跨域的问题**
- 在根目录下添加了`vue.config.js`文件，来解决跨域问题。详细内容请参考该文件