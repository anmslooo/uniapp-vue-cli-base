const config = require('./src/config.js')

module.exports = {
	// #ifdef H5
	devServer: {
		// 调试时允许内网穿透，让外网的人访问到本地调试的H5页面
		disableHostCheck: true,
		port: 8083,
		proxy: {
			'/': {
				target: config.HTTP_REQUEST_URL,
				changeOrigin: true,
				pathRewrite: {
					'^/': '/'
				}
			}
		},
	}
	// #endif
}
