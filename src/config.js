module.exports = {
	// 请求域名 格式：https://xxx.xxx.com
	HTTP_REQUEST_URL: 'http://localhost:8080',

	// 阿里云oss域名 格式：https://xxx.oss-cn-xxx.aliyuncs.com
	OSS_DOMAIN: '',

	// 文件直传到阿里云oss时需用到
	ACCESSKEYID: '',
	ACCESSKEYSECRET: '',

	// 与后端约定的token字段名
	TOKEN_NAME: 'Authorization'
}
